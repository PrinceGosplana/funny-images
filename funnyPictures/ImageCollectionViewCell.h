//
//  ImageCollectionViewCell.h
//  funnyPictures
//
//  Created by Oleksandr Isaiev on 10.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Giphy-iOS/AXCGiphy.h>

@interface ImageCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView * imageView;

- (void) configureCell:(AXCGiphy *) giphyData;

@end
