//
//  ImageCollectionViewCell.m
//  funnyPictures
//
//  Created by Oleksandr Isaiev on 10.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import "ImageCollectionViewCell.h"

@implementation ImageCollectionViewCell

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
      self.imageView.frame = self.frame;
    }
    return self;
}

- (void) configureCell:(AXCGiphy *) giphyData {
//  self.imageView.frame = self.frame;
//  NSLog(@"self  h %f w %f", self.frame.size.height, self.frame.size.width);
//  NSLog(@"imageView  h %f w %f", self.imageView.frame.size.height, self.imageView.frame.size.width);
//  NSLog(@"giphyData  h %f w %f", giphyData.originalImage.height, giphyData.originalImage.width);
//  self.imageView.contentMode = UIViewContentModeScaleAspectFill;
  
  NSURLRequest * request = [NSURLRequest requestWithURL:giphyData.originalImage.url];
  [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
    UIImage * image = [UIImage imageWithData:data];
//    NSLog(@"image  h %f w %f", image.size.height, image.size.width);
    [[NSOperationQueue mainQueue] addOperationWithBlock:^{
      self.imageView.image = image;
    }];
  }] resume];
}

- (void) prepareForReuse
{
    [super prepareForReuse];
    self.imageView.image = nil;
}


@end
