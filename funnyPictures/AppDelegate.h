//
//  AppDelegate.h
//  funnyPictures
//
//  Created by Oleksandr Isaiev on 10.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

