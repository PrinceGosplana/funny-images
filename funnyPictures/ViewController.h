//
//  ViewController.h
//  funnyPictures
//
//  Created by Oleksandr Isaiev on 10.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import <UIKit/UIKit.h>
@class Reachability;

@interface ViewController : UIViewController

@property (strong, nonatomic) NSMutableArray * giphyItems;
@property (nonatomic) NSInteger fullSize;
@property (strong, nonatomic) NSMutableArray * imagesArray;
@property(strong) Reachability * internetConnectionReach;

@property (weak, nonatomic) IBOutlet UIView *searchView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintTopPositionSearchView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *constraintPositionShowView;
@property (weak, nonatomic) IBOutlet UICollectionView * collectionView;
@property (weak, nonatomic) IBOutlet UIButton *btnSearchOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnCancelOutlet;
@property (weak, nonatomic) IBOutlet UIButton *btnShowSearchVewOutlet;
@property (weak, nonatomic) IBOutlet UITextField *searchTextFieldOutlet;
@property (weak, nonatomic) IBOutlet UIImageView *imageViewSwipe;
@property (weak, nonatomic) IBOutlet UIView *promptView;
@property (weak, nonatomic) IBOutlet UILabel * presentLabel;

- (IBAction)btnShowSearchViewPressed:(UIButton *)sender;
- (IBAction)btnCancelPressed:(UIButton *)sender;
- (IBAction)btnSearchPressed:(UIButton *)sender;
- (IBAction)showBtnToView:(UISwipeGestureRecognizer *)sender;
- (IBAction)showBtnFromView:(UISwipeGestureRecognizer *)sender;

@end

