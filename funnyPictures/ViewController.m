//
//  ViewController.m
//  funnyPictures
//
//  Created by Oleksandr Isaiev on 10.06.15.
//  Copyright (c) 2015 Oleksandr Isaiev. All rights reserved.
//

#import "ViewController.h"
#import <Giphy-iOS/AXCGiphy.h>
#import "ImageCollectionViewCell.h"
#import <SVProgressHUD.h>
#import "Reachability.h"

#define SECTION_INSETS_HEIGHT 10
#define SECTION_INSETS_WIDTH 10
#define LIMITLIMIT_LOADING_PICTURES 10
#define CORNER_RADIUS 10

NSString * const kCollectionViewCellIdentifier = @"cellReuseIdentifier";

@interface ViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UITextFieldDelegate>//, UICollectionViewDelegateFlowLayout>

@end

@implementation ViewController
- (UIStatusBarStyle) preferredStatusBarStyle {
  return UIStatusBarStyleLightContent;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // do not allow to scroll until you have pictures
    self.collectionView.scrollEnabled = NO;
    // hide collectioView - we animate the appearance later
    self.collectionView.alpha = 0.0;
    
    [self configureButtons];
    self.fullSize = 0;
    self.imagesArray = [NSMutableArray arrayWithCapacity:LIMITLIMIT_LOADING_PICTURES];
    // set your API key before making any requests. You may use kGiphyPublicAPIKey for development.
    [AXCGiphy setGiphyAPIKey:kGiphyPublicAPIKey];
  
    progress = 0.0f;
}


- (void) configureButtons {
  self.btnSearchOutlet.clipsToBounds = true;
  self.btnSearchOutlet.layer.cornerRadius = CORNER_RADIUS;
  
  self.btnCancelOutlet.clipsToBounds = true;
  self.btnCancelOutlet.layer.cornerRadius = CORNER_RADIUS;
  
  self.btnShowSearchVewOutlet.clipsToBounds = true;
  self.btnShowSearchVewOutlet.layer.cornerRadius = CORNER_RADIUS;
}

- (void) searchGIFs:(NSString *) searchText
{

      __unused  NSURLSessionDataTask * task = [AXCGiphy searchGiphyWithTerm:searchText limit:LIMITLIMIT_LOADING_PICTURES offset:0 completion:^(NSArray *results, NSError *error) {
      if (results) {
        
        self.giphyItems = [NSMutableArray arrayWithArray:results];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
          [self.collectionView reloadData];
        }];
      } else {
        [self alertViewControllerWithText:@"Image not found"];
        [self dismissError];
      }
    
    }];
}

#pragma mark - Touches

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.searchTextFieldOutlet resignFirstResponder];
}

#pragma mark - UICollectionViewDataSource

- (NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
  return  self.giphyItems.count;
}


- (UICollectionViewCell *) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    ImageCollectionViewCell * cell = [collectionView dequeueReusableCellWithReuseIdentifier:kCollectionViewCellIdentifier forIndexPath:indexPath];

  if (self.imagesArray.count && self.imagesArray.count > indexPath.row) {
                cell.imageView.image = [self.imagesArray objectAtIndex:indexPath.row];
  }
    return cell;
}

#pragma mark - UICollectionViewDelegateFlowLayout

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    AXCGiphy * gif = self.giphyItems[indexPath.item];
  if (self.imagesArray.count < LIMITLIMIT_LOADING_PICTURES) {
    [self downloadImages:gif];
  }
    if (gif != NULL) {
        CGSize sizeCollectionView = CGSizeMake(
                                               self.view.frame.size.width - (SECTION_INSETS_WIDTH + SECTION_INSETS_WIDTH) ,
                                               gif.fixedHeightImage.height - (SECTION_INSETS_HEIGHT + SECTION_INSETS_HEIGHT)
                                               );
        return sizeCollectionView;
    }
    return CGSizeZero;
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout insetForSectionAtIndex:(NSInteger)section {
    UIEdgeInsets sectionInsets = UIEdgeInsetsMake(SECTION_INSETS_HEIGHT, SECTION_INSETS_WIDTH, SECTION_INSETS_HEIGHT, SECTION_INSETS_WIDTH);
    return sectionInsets;
}
#pragma mark - UITextFieldDelegate

// called when 'return' key pressed
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    [self.searchTextFieldOutlet resignFirstResponder];
    return YES;
}

#pragma mark - Other methods

- (void) downloadImages: (AXCGiphy *) giphyImage {
    NSURLRequest * request = [NSURLRequest requestWithURL:giphyImage.originalImage.url];
    [[[NSURLSession sharedSession] dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
      
      if (data != nil) {
          UIImage * image = [UIImage imageWithData:data];
          [[NSOperationQueue mainQueue] addOperationWithBlock:^{
          [self.imagesArray addObject:image];
          NSLog(@"count loading %lu", (unsigned long)self.imagesArray.count);
          
          // get progress in loading
          progress =  (float)self.imagesArray.count / (float)self.giphyItems.count;
            
          // perform update for progressHUD
          [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.2f];
            
          // if all download - dismiss progress and show collectionView
          if (self.imagesArray.count == LIMITLIMIT_LOADING_PICTURES) {
            [self.collectionView reloadData];
            
            // animate showing collectionView
            [self showCollectionViewAnimate];
            // allow scroll in scrollView
            self.collectionView.scrollEnabled = YES;

            [self performSelector:@selector(dismissSuccess) withObject:nil afterDelay:0.3f];
          }
        }];
      }

    }] resume];
}

static float progress = 0.0f;

- (void)increaseProgress {
  [SVProgressHUD showProgress:progress status:@"Loading"];
}


#pragma mark - Dismiss Methods Sample

- (IBAction)dismissInfo{
  [SVProgressHUD showInfoWithStatus:@"Empty string"];
}

- (void)dismissSuccess {
  [SVProgressHUD showSuccessWithStatus:@"Great Success!"];
}

- (void)dismissError {
  [SVProgressHUD showErrorWithStatus:@"Failed with Error"];
}

- (IBAction)btnShowSearchViewPressed:(UIButton *)sender {
  
  self.constraintTopPositionSearchView.constant < 0 ? (self.constraintTopPositionSearchView.constant = 24) : (self.constraintTopPositionSearchView.constant = - 80);

  
  [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    
    self.btnShowSearchVewOutlet.transform = self.constraintTopPositionSearchView.constant > 0 ? CGAffineTransformMakeRotation(M_PI) : CGAffineTransformIdentity;
    [self.view layoutIfNeeded];
  } completion:^(BOOL finished) {
    
  }];
}

- (IBAction)btnCancelPressed:(UIButton *)sender {
    [self.searchTextFieldOutlet resignFirstResponder];
    [self btnShowSearchViewPressed:nil];
}

- (IBAction)btnSearchPressed:(UIButton *)sender {
  
  if ([self isConnected]) {
    // if searchTextFieldOutlet is not have text
    if (!self.searchTextFieldOutlet.text.length ) {
      [SVProgressHUD dismiss];
      [self alertViewControllerWithText:@"Empty data"];
    } else {
      // dismiss keyboard
      [self.searchTextFieldOutlet resignFirstResponder];
      
      // do searchint
      [self searchGIFs:self.searchTextFieldOutlet.text];
      
      progress = 0.0f;
      [SVProgressHUD showProgress:0 status:@"Loading"];
      [self performSelector:@selector(increaseProgress) withObject:nil afterDelay:0.3f];

      [self.imagesArray removeAllObjects];
      [self.giphyItems removeAllObjects];
      [self.collectionView reloadData];
    }
  } else {
    [self alertViewControllerWithText:@"Lost internet connection"];
  }
}

- (IBAction)showBtnToView:(UISwipeGestureRecognizer *)sender {
  

  
  self.constraintPositionShowView.constant = 20;
  [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    [self.view layoutIfNeeded];
    self.promptView.alpha = 0.0;
  } completion:^(BOOL finished) {
    
  }];
}

- (IBAction)showBtnFromView:(UISwipeGestureRecognizer *)sender {
  
  // if we see view for search - than we can't hide this button, onle after searchView will hide
  if (self.constraintTopPositionSearchView.constant < 0) {
    self.constraintPositionShowView.constant = -45;
  }

  float alphaForPromtView = 1.0;

  if (self.imagesArray.count) {
    alphaForPromtView = 0.0;
  }
  [UIView animateWithDuration:0.5 delay:0.0 usingSpringWithDamping:0.7 initialSpringVelocity:10 options:UIViewAnimationOptionCurveEaseInOut animations:^{
    [self.view layoutIfNeeded];
    self.promptView.alpha = alphaForPromtView;
  } completion:^(BOOL finished) {
    
  }];
}

- (void) showCollectionViewAnimate {
  self.collectionView.layer.anchorPoint = CGPointMake(0.5, 0.5);
  self.collectionView.transform = CGAffineTransformMakeScale(0.5, 0.5);
  self.collectionView.alpha = 0.0;
  
  [UIView animateWithDuration:1.0 delay:0.0 usingSpringWithDamping:0.4 initialSpringVelocity:15.0 options:UIViewAnimationOptionCurveEaseIn animations:^{
    self.collectionView.transform = CGAffineTransformIdentity;
    self.collectionView.alpha = 1.0;
  } completion:^(BOOL finished) {
    
  }];
}

- (void) alertViewControllerWithText: (NSString *) textMessage {
  UIAlertController * alert = [UIAlertController alertControllerWithTitle:@"Warning!" message:textMessage preferredStyle:UIAlertControllerStyleAlert];
  UIAlertAction* cancel = [UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction * action) {                                                     NSLog(@"cancel btn");
    [alert dismissViewControllerAnimated:YES completion:nil];
  }];
  [alert addAction:cancel];
  [self presentViewController:alert animated:YES completion:nil];
}

#pragma mark - chek internet connection

- (BOOL) isConnected {
  self.internetConnectionReach = [Reachability reachabilityForInternetConnection];
  self.internetConnectionReach.reachableOnWWAN = NO;
  
  if (self.internetConnectionReach.isReachable) {
    return YES;
  } else {
    return NO;
  }

}

@end
